/*
Copyright (C) 2011 J. Coliz <maniacbug@ymail.com>
 
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.
*/
 
/**
* Example for Getting Started with nRF24L01+ radios.
*
* This is an example of how to use the RF24 class. Write this sketch to two
* different nodes. Put one of the nodes into 'transmit' mode by connecting
* with the serial monitor and sending a 'T'. The ping node sends the current
* time to the pong node, which responds by sending the value back. The ping
* node can then see how long the whole cycle took.
*/
 
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

//
// Hardware configuration
//
 
// Set up nRF24L01 radio on SPI bus plus pins 9 & 10
 
//CE, CSN
//RF24 radio(5, 4);
 RF24 radio(7, 8);
//
// Topology
//
 
// Radio pipe addresses for the 2 nodes to communicate.
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

int delayTime=3000;
long totalEnergy = 100;

//output
double wirelessTransmitter = 0; //0.0;0.5;1.0
int listeningDecision = 1;
double lightDecision = 0; //0;0.5;1.0 (apagada, media, alta intensidade)

//input
int previousListeningDecision = 1;
double previousWirelessTransmitter = 0;
double previousLightDecision = 0;
double wirelessReceiver = 0; //0;1;2
double lightSensor = 0; //light sensor
double motionSensor = 0; //presence sensor


//led - port 13
//light - port 4
//motion - port 3
int ledPin = 13;
int led2Pin = 12;
//int lightPin = 4;
int lightPin = A1;
int motionPin = 3; // Input for HC-S501
//int motionPin = A2;

void setup() {
pinMode(ledPin, OUTPUT);
pinMode(led2Pin, OUTPUT);
pinMode(lightPin, INPUT);
pinMode(motionPin, INPUT);
digitalWrite(ledPin, HIGH);
digitalWrite(led2Pin, HIGH);
Serial.begin(57600);
radio.begin();
radio.setRetries(15,15);
radio.openWritingPipe(pipes[0]);
radio.openReadingPipe(1,pipes[0]);
}

void loop() {
getInputs();
delay(delayTime);
setOutputs();
 Serial.println("LightSensor");
 Serial.println(lightSensor);
 Serial.println("MotionSensor");
 Serial.println(motionSensor);
 Serial.println("WirelessTransmitter");
 Serial.println(wirelessTransmitter);
 Serial.println("listeningDecision");
 Serial.println(listeningDecision);
 Serial.println("lightDecision");
 Serial.println(lightDecision);
}

void getInputs(){
  lightSensor = readlightSensor();
  motionSensor = readMotionSensor();
  previousWirelessTransmitter = wirelessTransmitter; 
  previousListeningDecision = listeningDecision;
  previousLightDecision = lightDecision;
  if(listeningDecision==1){
    wirelessReceiver = receiveWirelessData();
    Serial.println("Received Signal");
    Serial.println(wirelessReceiver);
  }
  else 
  wirelessReceiver = 0;
}

  //double H0 = calculateH0Output();
  //double H1 = calculateH1Output();
void setOutputs(){
  double threshold1 = 0.4;
  double threshold2 = 0.7;
  double threshold3 = 0.6;
  double weightsH0[4] = { 1.2, -0.8, 1.6, -0.5};
  double weightsH1[4] = { 1.6, -0.8, 1.5, -0.3};
  double H0 = calculateHiddenUnitOutput(weightsH0);
  double H1 = calculateHiddenUnitOutput(weightsH1);
 // if(listeningDecision==0){
    double weightsTransmitterOutput[2] = { -0.6, -0.2 };
    double transmitterOutput = calculateOutputDecisions(weightsTransmitterOutput, H0, H1);
    if(transmitterOutput>threshold2){
      wirelessTransmitter = 1.0;
    }
    else{
      if(transmitterOutput>threshold1){
        wirelessTransmitter = 0.5;
      }
      else wirelessTransmitter = 0.0;
    }

    sendWirelessData(wirelessTransmitter);
 // }
  //else wirelessTransmitter = 0;
  double weightslisteningDecision[2] = { -0.9, -0.7};
  double listeningDecisionOutput = calculateOutputDecisions(weightslisteningDecision, H0, H1);
  if(listeningDecisionOutput>threshold3){
    listeningDecision = 1;
  }
  else{
    listeningDecision = 0;
  }

  double weightslightDecision[2] = { 1.7, -0.4};
  double lightDecisionOutput = calculateOutputDecisions(weightslightDecision, H0, H1);
  if(lightDecisionOutput>threshold2){
      lightDecision = 1.0;
   }
   else{
      if(lightDecisionOutput>threshold1){
        lightDecision = 0.5;
      }
      else lightDecision = 0.0;
   }
   //SET LED
   writeLed(lightDecision);
  
}


double calculateHiddenUnitOutput(double w[4]){
 double H = previousListeningDecision*w[0] +
  lightSensor*w[1]+motionSensor*w[2]+wirelessReceiver*w[3];
  double HOutput = fSigmoide(H);
  return HOutput;
}

double calculateOutputDecisions(double w[2], double h0, double h1){
  double outputSum = h0*w[0] + h1*w[1];
  double output = fSigmoide(outputSum);
  return output;
}

void writeLed(double value){
  if (value == 1){
    digitalWrite(ledPin, HIGH);
    digitalWrite(led2Pin, HIGH);
  }
  else if(value == 0.5){
    digitalWrite(ledPin, HIGH);
    digitalWrite(led2Pin, LOW);
  }
  else{
    digitalWrite(ledPin, LOW);
    digitalWrite(led2Pin, LOW);
  }
}


double fSigmoide(double x){
  double output = 1 / (1 + exp(-x));
  return output;
}

double readlightSensor(){
  double readValue = analogRead(lightPin);
  Serial.println(readValue);
  if(readValue < 10)
    return 1.0;
  else if (readValue>778)
     return 0.0;
  else 
    return  0.5;
  //return readValue;
}
double readMotionSensor(){
    double readValue = digitalRead(motionPin);
   // double readValue = analogRead(motionPin);
    return readValue;
}
long receiveWirelessData(){
  //Serial.println("Receive wireless data");
  radio.startListening();
  unsigned long started_waiting_at = millis();
  bool timeout = false;
  while ( ! radio.available() && ! timeout )
    if (millis() - started_waiting_at > (delayTime+400) )
    timeout = true;
   if ( timeout )
   {
      //Serial.println("Failed, response timed out.nr");
      return 0;
  }
  else
  {
    unsigned long got_time;
    radio.read( &got_time, sizeof(unsigned long) );
    return got_time;
  }
}



void sendWirelessData(long time){
//Serial.println("Send wireless data");
  // First, stop listening so we can talk.
  radio.stopListening();
  //Serial.println(time);
  bool ok = radio.write( &time, sizeof(unsigned long) );
}

